/* main.c recursive factorial implementation in LEGv8 assembly */

#include <stdio.h>

extern long long int fact(long long int n);

int main(void)
{
	long long int res = fact(6);
    printf("Result of fact(6) = %lld\n", res);
	res = fact(20);
    printf("Result of fact(20) = %lld\n", res);
    return 0;
}
